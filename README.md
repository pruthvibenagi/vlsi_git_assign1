
# Getting started with **git**
## Introduction 
This **git repository** is created for the demo

## Creating a local Repository : Command List
- `git init`
- `git config --local user.name YOUR_NAME`
- `git config --local user.email YOUR_EMAIL`
- add/changes some files
- `git status -s` or `git status`
- `git add` FILENAME(s)
- `git commit -m "commit msg description"`
- `git log`

## Commands essential for creating a local repo and commiting changes
- `git init`
- `git add` FILENAME(s)
- `git commit -m "commit msg description"`

## Commands optional for creating a local repo and commiting changes
- `git status -s` or `git status`
- `git log

## Git branches
- `git branch NEW`
- `git branch NEW ffddeeaa`
- `git branch -m NEW`
- `git branch -m OLD NEW`
- `git checkout NEW`
- `git switch NEW`
- `git branch -d NEW`
- `git push origin -delete OLD`
